/*
 * This file is part of the MicroPython project, http://micropython.org/
 *
 * The MIT License (MIT)
 *
 * Copyright (c) 2013, 2014 Damien P. George
 * Copyright (c) 2015 Josef Gajdusek
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <stdio.h>
#include <string.h>
#include <time.h>
#include "py/gc.h"
#include "py/runtime.h"
#include "py/mphal.h"
#include "py/smallint.h"
#include "SWM320.h"
#include "lib/timeutils/timeutils.h"
// #include "modmachine.h"
// #include "user_interface.h"
#include "extmod/utime_mphal.h"
/// \module time - time related functions
///
/// The `time` module provides functions for getting the current time and date,
/// and for sleeping.
/*test brench
import utime
print(utime.localtime())
set_time = utime.mktime((2020, 1, 29, 16, 31, 0, 2, 29))
print(utime.localtime(set_time))
print(utime.localtime())

*/
/// \function localtime([secs])
/// Convert a time expressed in seconds since Jan 1, 2000 into an 8-tuple which
/// contains: (year, month, mday, hour, minute, second, weekday, yearday)
/// If secs is not provided or None, then the current time from the RTC is used.
/// year includes the century (for example 2014)
/// month   is 1-12
/// mday    is 1-31
/// hour    is 0-23
/// minute  is 0-59
/// second  is 0-59
/// weekday is 0-6 for Mon-Sun.
/// yearday is 1-366
STATIC mp_obj_t time_localtime(size_t n_args, const mp_obj_t *args) {
    timeutils_struct_time_t tm;
    mp_int_t seconds;
    RTC_DateTime dateTime;
    if (n_args == 0 || args[0] == mp_const_none) {
        seconds = 666 / 1000 / 1000;
        RTC_GetDateTime(RTC, &dateTime);
        // uint32_t tm_wday=calcWeekDay(dateTime.Year, dateTime.Month, dateTime.Date);
        // tm.tm_year= dateTime.Year;
        // tm.tm_mon = dateTime.Month;
        // tm.tm_mday= dateTime.Date;
        // tm.tm_hour= dateTime.Hour;
        // tm.tm_min = dateTime.Minute;
        // tm.tm_sec = dateTime.Second;
        // tm.tm_wday= tm_wday;
        // tm.tm_yday= time()
        struct tm timestart;
        struct tm timeend;
        timestart.tm_sec=0;         /* 秒，范围从 0 到 59        */
        timestart.tm_min=0;         /* 分，范围从 0 到 59        */
        timestart.tm_hour=0;        /* 小时，范围从 0 到 23        */
        timestart.tm_mday=1;        /* 一月中的第几天，范围从 1 到 31    */
        timestart.tm_mon=0;         /* 月，范围从 0 到 11        */
        timestart.tm_year=2000-1900;        /* 自 1900 年起的年数        */
        time_t temp_tm_start = mktime(&timestart);
        timeend.tm_sec=dateTime.Second;         /* 秒，范围从 0 到 59        */
        timeend.tm_min=dateTime.Minute;         /* 分，范围从 0 到 59        */
        timeend.tm_hour=dateTime.Hour;        /* 小时，范围从 0 到 23        */
        timeend.tm_mday=dateTime.Date;        /* 一月中的第几天，范围从 1 到 31    */
        timeend.tm_mon=dateTime.Month;         /* 月，范围从 0 到 11        */
        timeend.tm_year=dateTime.Year-1900;        /* 自 1900 年起的年数        */
        time_t temp_tm_end = mktime(&timeend);
        seconds = difftime(temp_tm_end, temp_tm_start);
    } else {
        seconds = mp_obj_get_int(args[0]);
        timeutils_seconds_since_2000_to_struct_time(seconds, &tm);
        RTC_InitStructure RTC_initStruct;
        RTC_initStruct.Year   = tm.tm_year;
        RTC_initStruct.Month  = tm.tm_mon-1;
        RTC_initStruct.Date   = tm.tm_mday;
        RTC_initStruct.Hour   = tm.tm_hour;
        RTC_initStruct.Minute = tm.tm_min-1;
        RTC_initStruct.Second = tm.tm_sec-1;
        RTC_initStruct.SecondIEn = 0;
        RTC_initStruct.MinuteIEn = 0;
        RTC_Init(RTC, &RTC_initStruct);
        RTC_Start(RTC);
    }

    // RTC_initStruct.Year = 2016;
	// RTC_initStruct.Month = 5;
	// RTC_initStruct.Date = 5;
	// RTC_initStruct.Hour = 15;
	// RTC_initStruct.Minute = 5;
	// RTC_initStruct.Second = 5;
	// RTC_initStruct.SecondIEn = 0;
	// RTC_initStruct.MinuteIEn = 0;
	// RTC_Init(RTC, &RTC_initStruct);
	

    timeutils_seconds_since_2000_to_struct_time(seconds, &tm);
    mp_obj_t tuple[8] = {
        tuple[0] = mp_obj_new_int(tm.tm_year),
        tuple[1] = mp_obj_new_int(tm.tm_mon),
        tuple[2] = mp_obj_new_int(tm.tm_mday),
        tuple[3] = mp_obj_new_int(tm.tm_hour),
        tuple[4] = mp_obj_new_int(tm.tm_min),
        tuple[5] = mp_obj_new_int(tm.tm_sec),
        tuple[6] = mp_obj_new_int(tm.tm_wday),
        tuple[7] = mp_obj_new_int(tm.tm_yday),
    };
    return mp_obj_new_tuple(8, tuple);
}
MP_DEFINE_CONST_FUN_OBJ_VAR_BETWEEN(time_localtime_obj, 0, 1, time_localtime);

/// \function mktime()
/// This is inverse function of localtime. It's argument is a full 8-tuple
/// which expresses a time as per localtime. It returns an integer which is
/// the number of seconds since Jan 1, 2000.
STATIC mp_obj_t time_mktime(mp_obj_t tuple) {
    size_t len;
    mp_obj_t *elem;
    mp_obj_get_array(tuple, &len, &elem);

    // localtime generates a tuple of len 8. CPython uses 9, so we accept both.
    if (len < 8 || len > 9) {
        nlr_raise(mp_obj_new_exception_msg_varg(&mp_type_TypeError, "mktime needs a tuple of length 8 or 9 (%d given)", len));
    }

    return mp_obj_new_int_from_uint(timeutils_mktime(mp_obj_get_int(elem[0]),
            mp_obj_get_int(elem[1]), mp_obj_get_int(elem[2]), mp_obj_get_int(elem[3]),
            mp_obj_get_int(elem[4]), mp_obj_get_int(elem[5])));
}
MP_DEFINE_CONST_FUN_OBJ_1(time_mktime_obj, time_mktime);

/// \function time()
/// Returns the number of seconds, as an integer, since 1/1/2000.
STATIC mp_obj_t time_time(void) {
    // get date and time
    return mp_obj_new_int(666 / 1000 / 1000);
}
MP_DEFINE_CONST_FUN_OBJ_0(time_time_obj, time_time);

STATIC const mp_rom_map_elem_t time_module_globals_table[] = {
    { MP_ROM_QSTR(MP_QSTR___name__), MP_ROM_QSTR(MP_QSTR_utime) },
    { MP_ROM_QSTR(MP_QSTR_localtime), MP_ROM_PTR(&time_localtime_obj) },
    { MP_ROM_QSTR(MP_QSTR_mktime), MP_ROM_PTR(&time_mktime_obj) },
    { MP_ROM_QSTR(MP_QSTR_sleep), MP_ROM_PTR(&mp_utime_sleep_obj) },
    { MP_ROM_QSTR(MP_QSTR_sleep_ms), MP_ROM_PTR(&mp_utime_sleep_ms_obj) },
    { MP_ROM_QSTR(MP_QSTR_sleep_us), MP_ROM_PTR(&mp_utime_sleep_us_obj) },
    { MP_ROM_QSTR(MP_QSTR_ticks_ms), MP_ROM_PTR(&mp_utime_ticks_ms_obj) },
    { MP_ROM_QSTR(MP_QSTR_ticks_us), MP_ROM_PTR(&mp_utime_ticks_us_obj) },
    { MP_ROM_QSTR(MP_QSTR_ticks_cpu), MP_ROM_PTR(&mp_utime_ticks_cpu_obj) },
    { MP_ROM_QSTR(MP_QSTR_ticks_add), MP_ROM_PTR(&mp_utime_ticks_add_obj) },
    { MP_ROM_QSTR(MP_QSTR_ticks_diff), MP_ROM_PTR(&mp_utime_ticks_diff_obj) },
    { MP_ROM_QSTR(MP_QSTR_time), MP_ROM_PTR(&time_time_obj) },
};

STATIC MP_DEFINE_CONST_DICT(time_module_globals, time_module_globals_table);

const mp_obj_module_t utime_module = {
    .base = { &mp_type_module },
    .globals = (mp_obj_dict_t*)&time_module_globals,
};
