
BUILD ?= build
.PHONY:all
include ../../py/mkenv.mk

# qstr definitions (must come before including py.mk)
QSTR_DEFS = qstrdefsport.h

# FROZEN_MPY_DIR ?= buildin-py

# include py core make definitionsvers/uarths.c
include $(TOP)/py/py.mk
#
# You must pass CROSS_COMPILE and PLATFORM variable.
#
# CROSS_COMPILE ?= arm-none-eabi-
# CC      = $(CROSS_COMPILE)gcc
# OBJCOPY = $(CROSS_COMPILE)objcopy
# OBJDUMP = $(CROSS_COMPILE)objdump


CFLAGS = -mthumb -mcpu=cortex-m4 -mfloat-abi=soft -std=c99 -Wpointer-arith
CFLAGS +=  -ffunction-sections -fdata-sections -fno-common -funsigned-char -mno-unaligned-access

DEBUG = 1
ifeq ($(DEBUG),1)
CFLAGS += -O0 -g
else
CFLAGS += -O2
endif

ASFLAGS = $(CFLAGS)

LINKER_SCRIPT = swm320.ld
LDFLAGS = -Wl,--gc-sections -Wl,-Map=$@.map -T $(LINKER_SCRIPT) -mthumb -mcpu=cortex-m4 -mfloat-abi=soft -specs=nosys.specs -specs=nano.specs -u _printf_float

INC += -I.
INC += -I$(TOP)
INC += -I$(BUILD)
INC += -IAPP/
INC += -ICSL/CMSIS/CoreSupport/
INC += -ICSL/CMSIS/DeviceSupport/
INC += -ICSL/SWM320_StdPeriph_Driver/
INC += -IFreeRTOS/
INC += -IFreeRTOS/port/
CFLAGS += $(INC)
CFLAGS += -DFFCONF_H=\"$(OOFATFS_DIR)/ffconf.h\"
APP_SRC = main.c \
		mphalport.c \
		help.c \
		delay.c \
		modutime.c \
		modflash.c \
		moduos.c \
		FreeRTOS/croutine.c \
		FreeRTOS/event_groups.c \
		FreeRTOS/heap/heap_1.c \
		FreeRTOS/list.c \
		FreeRTOS/port/port.c \
		FreeRTOS/queue.c \
		FreeRTOS/tasks.c \
		FreeRTOS/timers.c \
		modmachine.c \
		pybpin.c \
		bufhelper.c \
		pybspi.c \

CSL_SRC = $(addprefix CSL/,\
	CMSIS/DeviceSupport/system_SWM320.c \
	SWM320_StdPeriph_Driver/SWM320_adc.c \
	SWM320_StdPeriph_Driver/SWM320_can.c \
	SWM320_StdPeriph_Driver/SWM320_crc.c \
	SWM320_StdPeriph_Driver/SWM320_dma.c \
	SWM320_StdPeriph_Driver/SWM320_exti.c \
	SWM320_StdPeriph_Driver/SWM320_flash.c \
	SWM320_StdPeriph_Driver/SWM320_gpio.c \
	SWM320_StdPeriph_Driver/SWM320_i2c.c \
	SWM320_StdPeriph_Driver/SWM320_lcd.c \
	SWM320_StdPeriph_Driver/SWM320_norflash.c \
	SWM320_StdPeriph_Driver/SWM320_port.c \
	SWM320_StdPeriph_Driver/SWM320_pwm.c \
	SWM320_StdPeriph_Driver/SWM320_rtc.c \
	SWM320_StdPeriph_Driver/SWM320_sdio.c \
	SWM320_StdPeriph_Driver/SWM320_sdram.c \
	SWM320_StdPeriph_Driver/SWM320_spi.c \
	SWM320_StdPeriph_Driver/SWM320_sram.c \
	SWM320_StdPeriph_Driver/SWM320_timr.c \
	SWM320_StdPeriph_Driver/SWM320_uart.c \
	SWM320_StdPeriph_Driver/SWM320_wdt.c \
)

CSL_SRS = $(addprefix CSL/,\
	CMSIS/DeviceSupport/startup/gcc/startup_SWM320.s \
)

LIB_SRC_C ?=
MICROPY_FATFS=0
ifeq ($(MICROPY_FATFS), 1)
LIB_SRC_C += \
lib/oofatfs \
lib/oofatfs/option
SRCDIRS += $(LIB_SRC_C)
endif

MPY_CFILES    += lib/timeutils/timeutils.c \
		 		lib/utils/sys_stdio_mphal.c \
		 		lib/mp-readline/readline.c \
				lib/utils/stdout_helpers.c \
				lib/utils/interrupt_char.c \
				lib/utils/pyexec.c \
				lib/libm/nearbyintf.c \
				lib/libm_dbl/nearbyint.c \
				lib/netutils/netutils.c \
				lib/oofatfs/ff.c \
				lib/oofatfs/ffunicode.c \
				lib/libc/string0.c

PY_O		+=	$(patsubst %, $(BUILD)/%, $(MPY_CFILES:.c=.o))
LIB_SRC_C = $(MPY_CFILES)

# List of sources for qstr extraction
SRC_QSTR +=  $(APP_SRC) $(MPY_CFILES)
# Append any auto-generated sources that are needed by sources listed in SRC_QSTR
SRC_QSTR_AUTO_DEPS +=

SRC_C += $(APP_SRC) $(CSL_SRS)

TARGET ?= SWM320_MPY

OBJS = $(addprefix $(BUILD)/, $(APP_SRC:.c=.o) $(CSL_SRC:.c=.o) $(CSL_SRS:.s=.o))


all: $(BUILD)/$(TARGET).elf

$(BUILD)/_frozen_mpy.o:$(BUILD)/_frozen_mpy.c
	mkdir -p $(BUILD)/py
	mkdir -p $(BUILD)/extmod
	mkdir -p $(BUILD)/lib/embed/
	mkdir -p $(BUILD)/lib/utils/
	mkdir -p $(BUILD)/lib/timeutils/
	mkdir -p $(BUILD)/lib/libc/
	mkdir -p $(BUILD)/lib/mp-readline/
	mkdir -p $(BUILD)/lib/netutils/
	mkdir -p $(BUILD)/lib/libm/
	mkdir -p $(BUILD)/lib/libm_dbl/
	mkdir -p $(BUILD)/lib/oofatfs
	$(CC) $(CFLAGS) -MD -MP -MF $@.d -c $< -o $@

$(BUILD)/_frozen_mpy.c: frozentest.mpy $(BUILD)/genhdr/qstrdefs.generated.h
	$(ECHO) "MISC freezing bytecode"
	$(Q)$(TOP)/tools/mpy-tool.py -f -q $(BUILD)/genhdr/qstrdefs.preprocessed.h -mlongint-impl=none $< > $@


$(BUILD)/$(TARGET).elf: $(BUILD)/_frozen_mpy.o $(OBJS) $(PY_O)
	$(CC) -o $@ $(LDFLAGS) $(OBJS) $(PY_O) $(BUILD)/_frozen_mpy.o -lm
	$(OBJCOPY) -O ihex   $@ $(BUILD)/$(TARGET).hex
	$(OBJCOPY) -O binary $@ $(BUILD)/$(TARGET).bin
	$(SIZE) $@
	# $(OBJDUMP) -d $@ > $(BUILD)/$(TARGET).elf.dis

# $(BUILD)/%.o: %.c
# 	echo "Compiling $^"
# 	$(CC) -c -o $@ $(CFLAGS) $<

# $(BUILD)/%.o: %.s
# 	echo "Assembling $^"
# 	$(CC) -c -o $@ $(ASFLAGS) $<


# $(sort $(var)) removes duplicates
#
# The net effect of this, is it causes the objects to depend on the
# object directories (but only for existence), and the object directories
# will be created if they don't exist.
OBJ_DIRS = $(sort $(dir $(OBJS)))
$(OBJS): | $(OBJ_DIRS)
$(OBJ_DIRS):
	mkdir -p $@


#clean:
#	rm -rf $(BUILD)/*

include $(TOP)/py/mkrules.mk
